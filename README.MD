### Build procedure

Install npm packages via the following command:

```
npm install
```

Build javascript and sass assets (uses gulp):

```
npm run build
```

Start http-server (should open browser at http://127.0.0.1:8080):

```
npm start
```

NOTE: 
On Windows might require http-server to be installed globally:
```
npm install http-server -g
```