(function () {
    'use strict';

    angular
        .module('app')
        .directive('uniqueName', uniqueName);

    function uniqueName() {
        var directive = {
            restrict: "A",
            require: "ngModel",
            scope: {
                uniqueName: '='
            },
            link: function (scope, element, attributes, ngModel) {
                ngModel.$validators.uniqueName = function (modelValue) {
                    var array = scope.uniqueName || [];
                    var len = array.length;
                    for(var i = 0; i < len; i++) {
                        if(modelValue === array[i].name) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        };

        return directive;
    }
})();