(function() {
    'use strict';

    angular
        .module('app')
        .directive('addProject', addProject);

    addProject.$inject = [];
    function addProject() {
        var directive = {
            bindToController: true,
            controller: AddProjectController,
            templateUrl: './app/components/add-project.directive.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {
                projects: '=',
                isDisplayed: '='
            }
        };
        return directive;
        
        function link(scope, element, attrs) {
            
        }
    }

    function AddProjectController () {
        var vm = this;

        vm.newProject = {};

        vm.addNewProject = addNewProject;

        vm.close = close;

        ////////////////

        function addNewProject(project) {
            project.positions = [];
            project.isOpen = true;

            vm.projects.push(project);
            close();
        };

        function close() {
            vm.isDisplayed = false;
            vm.newProject = {};
        }
    }
})();