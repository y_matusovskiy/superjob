(function() {
    'use strict';

    angular
        .module('app')
        .directive('addPosition', addPosition);

    addPosition.$inject = [];
    function addPosition() {
        var directive = {
            bindToController: true,
            controller: AddPositionController,
            templateUrl: './app/components/add-position.directive.html',
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {
                project: '=',
                isDisplayed: '='
            }
        };
        return directive;
        
        function link(scope, element, attrs) {
            
        }
    }

    function AddPositionController () {
        var vm = this;

        vm.newPosition = {};

        vm.addNewPosition = addNewPosition;

        vm.close = close;

        ////////////////

        function addNewPosition(position) {
            position.isOpen = true;            
            vm.project.positions = vm.project.positions || [];
            vm.project.positions.push(position);
            close();
        };

        function close() {
            vm.isDisplayed = false;
            vm.newPosition = {};
        }
    }
})();