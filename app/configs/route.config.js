(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/projects', {
                templateUrl: './app/projects/projects.html',
                controller: 'ProjectsController',
                controllerAs: 'vm'
            })
            .when('/', { redirectTo: '/projects' })
            .otherwise({ redirectTo: '/' });
    }
})();