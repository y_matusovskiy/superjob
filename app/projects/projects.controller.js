(function () {
    'use strict';

    angular
        .module('app')
        .controller('ProjectsController', ProjectsController);

    ProjectsController.$inject = ['ProjectsSerivce'];
    function ProjectsController(ProjectsSerivce) {
        var vm = this;

        vm.projects = [];

        vm.selectedProject = {};

        vm.filter = {
            name: '',
            onlyOpen: undefined
        }

        vm.displayAddProjectModal = false;
        vm.displayAddPositionModal = false;

        vm.expandProject = expandProject;

        vm.openAddProjectModal = openAddProjectModal;
        vm.openAddPositionModal = openAddPositionModal;

        vm.openProject = openProject;
        vm.openPosition = openPosition;

        vm.closeProject = closeProject;
        vm.closePosition = closePosition;

        vm.removeProject = removeProject;
        vm.removePosition = removePosition;

        vm.projectsFilter = projectsFilter;
        vm.positionsFilter = positionsFilter;

        vm.getNumberEnding = getNumberEnding;

        vm.getNumberOfPositions = getNumberOfPositions;

        activate();

        ////////////////

        function activate() {
            ProjectsSerivce.getProjects()
                .then(function (res) {
                    vm.projects = res.data;
                }, function (err) {

                });
        }

        function expandProject(selectedProject) {
            angular.forEach(vm.projects, function (project) {
                project.isExpanded = project === selectedProject;
            });
        }

        function openAddProjectModal() {
            vm.displayAddProjectModal = true;
        }

        function openAddPositionModal(project) {
            vm.selectedProject = project;

            vm.displayAddPositionModal = true;
        }

        function openProject(project) {
            project.isOpen = true;
        }

        function openPosition(project, position) {
            // project has to be open in order to open a position
            project.isOpen = true;

            position.isOpen = true;
        }

        function closeProject(project) {
            project.isOpen = false;

            angular.forEach(project.positions, function (project) {
                project.isOpen = false;
            });
        }

        function closePosition(project, position) {
            position.isOpen = false;

            if (checkIfAllPositionsClosed(project)) {
                project.isOpen = false;
            }
        }

        function removeProject(project) {
            var idx = vm.projects.indexOf(project);
            vm.projects.splice(idx, 1);
        }

        function removePosition(project, position) {
            var idx = project.positions.indexOf(position);
            project.positions.splice(idx, 1);
        }

        function projectsFilter(project) {
            var positions = project.positions || [];
            var len = positions.length;

            // always show empty projects if filter is blank
            if (len === 0 && !vm.filter.name && !vm.filter.onlyOpen) {
                return true;
            }

            for (var i = 0; i < len; i++) {
                if (positionsFilter(positions[i])) {
                    return true;
                }
            }

            return false;
        }

        function positionsFilter(position) {
            return (vm.filter.onlyOpen ? position.isOpen === true : true)
                && position.name.toLowerCase().indexOf(vm.filter.name.toLowerCase()) !== -1;
        }

        // generates proper ending depending on the given number
        function getNumberEnding(number, endings) {
            var ending, i;
            number = number % 100;
            if (number >= 11 && number <= 19) {
                ending = endings[2];
            }
            else {
                i = number % 10;
                switch (i) {
                    case (1): ending = endings[0]; break;
                    case (2):
                    case (3):
                    case (4): ending = endings[1]; break;
                    default: ending = endings[2];
                }
            }
            return ending;
        }

        function getNumberOfPositions(project) {
            return (project.positions || []).length;
        }

        ////////////////

        function checkIfAllPositionsClosed(project) {
            var len = project.positions.length;

            for (var i = 0; i < len; i++) {
                if (project.positions[i].isOpen) {
                    return false;
                }
            }

            return true;
        }
    }
})();