(function () {
    'use strict';

    angular
        .module('app')
        .service('ProjectsSerivce', ProjectsSerivce);

    ProjectsSerivce.$inject = ['$http'];
    function ProjectsSerivce($http) {
        this.getProjects = getProjects;

        ////////////////

        function getProjects() {
            return $http.get('/data/projects.json');
        }
    }
})();