'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var argv = require('yargs').argv;

var config = {
    sourceMaps: !argv.production
};

gulp.task('sass', function () {
    return gulp.src('./styles/scss/**/*.scss')
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write()))
        .pipe(gulp.dest('./dist'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./styles/scss/**/*.scss', ['sass']);
});

gulp.task('js', function () {
    return gulp.src('./app/**/*.js')
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write()))
        .pipe(gulp.dest('./dist'));
});

gulp.task('js:watch', function () {
    gulp.watch('./app/**/*.js', ['js']);
});

gulp.task('watch', ['sass:watch', 'js:watch']);

gulp.task('default', ['sass', 'js']);